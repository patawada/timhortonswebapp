package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

public class MealsServiceTest {

	/*
	 * @BeforeClass public static void setUpBeforeClass() throws Exception { }
	 * 
	 * @AfterClass public static void tearDownAfterClass() throws Exception { }
	 * 
	 * @Before public void setUp() throws Exception { }
	 * 
	 * @After public void tearDown() throws Exception { }
	 * 
	 * @Test public void test() { fail("Not yet implemented"); }
	 */

	@Test
	public void testDrinkTypes() {

		List<String> types = MealsService.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("The list is empty", types.isEmpty() == false);
	}

	@Test
	public void testDrinkTypesException() {

		List<String> types = MealsService.getAvailableMealTypes(MealType.DRINKS);
		boolean isTrue = types.get(0).equals("None");
		assertTrue("The list is empty", isTrue == false);
	}

	@Test
	public void testDrinkTypesIn() {

		List<String> types = MealsService.getAvailableMealTypes(MealType.DRINKS);
		int numOfDrinks = types.size();
		assertTrue("The list is more than 3", types.size() > 3);
	}

	@Test
	public void testDrinkTypesOut() {

		List<String> types = MealsService.getAvailableMealTypes(MealType.DRINKS);
		int numOfDrinks = types.size();
		assertFalse("The list is less than 1", types.size() < 1);
	}

}
